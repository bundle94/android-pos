//package com.interswitchng.interswitchpossdkdemo.activities
//
//import android.os.Bundle
//import android.util.Log
//import android.view.MenuItem
//import android.view.View
//import androidx.appcompat.app.ActionBarDrawerToggle
//import androidx.appcompat.app.AppCompatActivity
//import com.interswitchng.interswitchpossdkdemo.R
//import com.interswitchng.smartpos.IswPos
//import com.interswitchng.smartpos.shared.models.results.IswTransactionResult
//import com.interswitchng.smartpos.shared.utilities.toast
//import com.interswitchng.smartpos.shared.interfaces.retrofit.IKimonoHttpService
//import kotlinx.android.synthetic.main.activity_transfer.*
//
//internal class TransferActivity(private val httpService: IKimonoHttpService) : AppCompatActivity(), IswPos.IswPaymentCallback {
//
//    private lateinit var drawerToggle: ActionBarDrawerToggle
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_transfer)
//        setSupportActionBar(findViewById(R.id.toolbar))
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        if (item.itemId == android.R.id.home) {
//            finish()
//            return true
//        }
//        return super.onOptionsItemSelected(item)
//    }
//
//    override fun onUserCancel() {
//        toast("User cancelled payment")
//    }
//
//    override fun onPaymentCompleted(result: IswTransactionResult) {
//        Log.d("Demo", "" + result)
//
//        // reset the amount back to default
//        //if (result.isSuccessful) onTextChange(defaultAmount)
//        val message =
//                if (result.responseCode == "00") "Payment completed successfully" else "Payment cancelled"
//        toast(message)
//    }
//
//    fun processTransfer(view: View) {
//        Log.d("Demo", "Inside process transfer method")
//    }
//}