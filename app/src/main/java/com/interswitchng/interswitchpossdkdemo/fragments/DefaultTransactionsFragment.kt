package com.interswitchng.interswitchpossdkdemo.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.interswitchng.interswitchpossdkdemo.R
import com.interswitchng.interswitchpossdkdemo.activities.CashBackActivity
import com.interswitchng.interswitchpossdkdemo.activities.HomeActivity
import com.interswitchng.interswitchpossdkdemo.activities.KeypadActivity
import com.interswitchng.interswitchpossdkdemo.models.TransactionTypes
import com.interswitchng.interswitchpossdkdemo.adapters.TransactionTypesAdapter
import kotlinx.android.synthetic.main.fragment_default_transactions.*
import java.text.SimpleDateFormat
import java.util.*

class DefaultTransactionsFragment: Fragment() {

    private val parentActivity: HomeActivity get() = activity as HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_default_transactions, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // get list of default transactions
        val defaultTransactions = TransactionTypes.getDefault(view.context)

        // set adapter for recycler view
        rvTransactionTypes.adapter = TransactionTypesAdapter(defaultTransactions) {

            if (it == TransactionTypes.More) {
                parentActivity.showMore()
            }else if(it == TransactionTypes.CashBack){
                val intent = Intent(requireContext(), CashBackActivity::class.java)
                        .putExtra("TRANSACTION_TYPE", it.toTransactionType.name)

                startActivity(intent)
            }else {
                // create intent with transaction type
                val intent = Intent(requireContext(), KeypadActivity::class.java)
                    .putExtra(KeypadActivity.KEY_TYPE, it.toTransactionType.name)

                // start demo activity
                startActivity(intent)
            }
        }

        val formatter = SimpleDateFormat ("EEE, MMM d", Locale.ROOT)
        tvDate.text = formatter.format(Date())
    }
}